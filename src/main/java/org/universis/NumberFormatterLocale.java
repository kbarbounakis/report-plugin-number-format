package org.universis;

import java.util.HashMap;
import java.util.Map;

public class NumberFormatterLocale {
    public Map<String,LocaleValue> values = new HashMap<String, LocaleValue>();
    public String decimalSeparator = null;
    public Map<String,String> dictionary = new HashMap<String, String>();
    public String spellcheck(String text) {
        for (Map.Entry<String,String> entry : this.dictionary.entrySet()) {
            String key = entry.getKey();
            if (key.startsWith("/") && key.endsWith("/")) {
                // replace with regular expression
                text = text.replaceAll(key.substring(1,key.length()-1), entry.getValue());
            } else {
                text = text.replace(entry.getKey(), entry.getValue());
            }

        }
        return text;
    }
}
